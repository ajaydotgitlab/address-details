/*
samples 
Peking University health Science Centre, Beijing, beijing, china

Huadong Hospital Affilated to fudan University, Shanghai, Shanghai, China

Sichuan Univeristy, Chengdu, Sichuan, China

Sun Yat-Sen University, Guandzhou, Guangdong, China

Yangzhou University, Yamgzhou, Jiangsu, China

Department of Epidemiology and Biostatistics, Peking University Health Science Centre, Beijing, Beijing, China

National Institute for Communicable Disease Control and Prevention, Beijing, China

Peking University First Hospital, Beijing, China

Duke University Medical Center, Durham, North Carolina, United States

Chinese Preventive Medicine Association, Beijing, China

China National Center for Food Safety Risk Assessment, Beijing, China

Epidemiology, Peking University Health Science Centre, Beijing, Beijing, China

Psychology Department, University of Haifa, Haifa, Israel

let address = [
  "Peking University health Science Centre, Beijing, beijing, china",
  "Huadong Hospital Affilated to fudan University, Shanghai, Shanghai, China",
  "Sichuan Univeristy, Chengdu, Sichuan, China",
  "Sun Yat-Sen University, Guandzhou, Guangdong, China",
  "Yangzhou University, Yamgzhou, Jiangsu, China",
  "Department of Epidemiology and Biostatistics, Peking University Health Science Centre, Beijing, Beijing, China",
  "National Institute for Communicable Disease Control and Prevention, Beijing, China",
  "Peking University First Hospital, Beijing, China",
  "Duke University Medical Center, Durham, North Carolina, United States",
  "Chinese Preventive Medicine Association, Beijing, China",
  "China National Center for Food Safety Risk Assessment, Beijing, China",
  "Epidemiology, Peking University Health Science Centre, Beijing, Beijing, China",
  "Psychology Department, University of Haifa, Haifa, Israel",
  "Fake address, Chennai, tamilnadu, india",
];
*/
/*
// adding countries to database
for (let i = 0; i < splittedAddress.length; i++) {
  // for(let j = 0; j < splittedAddress[i].length; j++){
  //     countriesDb[i] = splittedAddress[i][splittedAddress[i].length]
  // }
  let j = splittedAddress[i].length - 1;
  countriesDb[i] = splittedAddress[i][j];
}
*/

let address = [
  "Epidemiology, Peking University Health Science Centre, Anhui, Beijing, China",
  "Psychology Department, University of Haifa, Haifa, Israel",
  "Fake address, Chennai, tamilnadu, india",
];
const data = require("./countries+states+cities.json");

// countries database
let countriesDb = [];
for (let i = 0; i < data.length; i++) {
  countriesDb.push(data[i].name);
}

let statesDb = [];
let citiesDb = [];
let splittedAddress = [];
let addressObj = {
  id: "",
  fullAddress: "",
  department: "",
  university: "",
  city: "",
  state: "",
  country: "",
};
let allAdressesObj = [];

// splits the address with respect to comma(,)
for (let i = 0; i < address.length; i++) {
  let temp = address[i];
  let singleAddressElementsArr = [];
  temp = temp.toLowerCase();
  singleAddressElementsArr = temp.split(",");

  //removes whites spaces and uppercase first letter
  for (let j = 0; j < singleAddressElementsArr.length; j++) {
    singleAddressElementsArr[j] = singleAddressElementsArr[j].trim();
    singleAddressElementsArr[j] =
      singleAddressElementsArr[j].charAt(0).toUpperCase() +
      singleAddressElementsArr[j].slice(1);
  }
  splittedAddress[i] = singleAddressElementsArr;
}

for (let i = 0; i < splittedAddress.length; i++) {
  for (let j = 0; j < splittedAddress[i].length; j++) {
    for (let k = 0; k < countriesDb.length; k++) {
      if (splittedAddress[i][j] == countriesDb[k]) {
        console.log(splittedAddress[i][j]);
        addressObj.country = countriesDb[k];
      }
    }
  }
  addressObj.fullAddress = splittedAddress[i];
  addressObj.id = i;
  console.log(addressObj);
}
